import yaml, os, glob, re, sys

try:
    import textwrap
    textwrap.indent
except AttributeError:  # undefined function (wasn't added until Python 3.3)
    def indent(text, amount, ch=' '):
        padding = amount * ch
        return ''.join(padding+line for line in text.splitlines(True))
else:
    def indent(text, amount, ch=' '):
        return textwrap.indent(text, amount * ch)

pattern = re.compile("configs/(?P<env>dev|test|prod)\.(?P<app>\w+).cfg")

# This should be prod
files = glob.glob('configs/test.*.cfg')

f=sys.argv[1]
s = file(f, 'r')
m = re.match(pattern, f)
app = m.group('app')
env = m.group('env')

prefix = app
mnFn = prefix + ".manifest.yml"
mnFile = file(mnFn, 'w')
sFn = prefix + ".secrets.yml"
sFile = file(sFn, 'w')
autoWarn = "# This section gets generated. Do not edit\n"
mnWarn = indent(autoWarn, 12)
sWarn = indent(autoWarn, 2)
mnFile.write(mnWarn)
sFile.write(sWarn)

for data in yaml.load_all(s):
    values = data[app]
    for k in values.keys():
        manifestEntry="""- name: \"%s\"
  valueFrom:
    secretKeyRef:
      name: \"{{ env }}-{{ prefix }}-{{ application }}\"
      key: \"%s\"\n""" % (k.upper(),  k.replace("_",".").lower())
            
        secretsEntry="""%s: \"{{ %s.%s | b64encode }}\"\n""" % (k, app, k)
    
        mnFile.write(indent(manifestEntry,12))
        sFile.write(indent(secretsEntry,2))

    mnFile.write(mnWarn)
    sFile.write(sWarn)
    mnFile.close()
    sFile.close()

print(mnFn + " " + sFn)

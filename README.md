# README #

Secrets stores secrets and (for now) configuration for skedulo applications. It does so using [Blackbox](https://github.com/StackExchange/blackbox). 
## Setup secrets ##
### Install blackbox ###
On MacOS simply `brew install blackbox`. For linux and others please see [here](https://github.com/StackExchange/blackbox#installation-instructions)
### Add secrets to your repository ###
If you want to use secrets in your project you will need to add it as a submodule:
```bash
git submodule add git@bitbucket.org:skedulo/secrets.git
git commit -m "Added secrets submodule"
```
When cloning into your repo you will have to
```bash
git clone --recursive <repo>
```
More information on how to work with submodules can be found [here](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
### Get onboarded ###
Follow the steps outlined in [this](https://github.com/StackExchange/blackbox#step-1-you-create-a-gpg-key-pair-on-a-secure-machine-and-add-to-public-keychain) section.
## Use secrets ##
### Process ###
Using secrets requires following a simple process:
1) Add/Modify/Delete secrets on your branch
2) Create a pull request to master when done and build succeeds
3) Create a pull request to master in `ansible-skedulo-kube`. The branch name will be the same as your secrets branch name
4) Merge
Note: Binary files can't be merged - make sure only one person is working on one application config at a time. 
### Adding a configuration ###
Config files, by conventions, are of the shape `<dev|test|prod>.<application>.cfg`.They live in the `configs/` directory. Secrets will not build properly if this convention is violated. To add a file
```bash
blackbox_register_new_file configs/<dev|test|prod>.<app>.cfg
```
This will ask for your password and encrypt the file. It will also update the `.gitignore` file but, due to a bug in blackbox, not add it automatically. I have a fix and will submit a patch but unless this is fixed upstream, don't forget to add the `.gitignore` before you push:
```bash
git commit --amend --no-edit
```
### Modifying an existing configuration ###
Simply
```
blackbox_edit configs/<dev|test|prod>.<app>.cfg
git commit
```
### Deleting an existing configuration ###
Deleting a configuration is equally straight forward: 
```bash
blackbox_deregister_file configs/<dev|test|prod>.<app>.cfg
git commit --amend --no-edit
```
### Advanced usage ###
The above instructions should do most if not all you will ever need. For a full set of commands, see [this](https://github.com/StackExchange/blackbox).
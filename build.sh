#! /bin/bash

#set -e

ANSIBLE_DIR=ansible-skedulo-kube

function split {
    OFS=IFS
    IFS=$2 read -ra CONSTITUENTS <<< "$1"
    IFS=OFS
    echo ${CONSTITUENTS[@]}
}

function git_rm {
    cd $ANSIBLE_DIR
    git rm $1
    cd ../
}

function git_add {
    cd $ANSIBLE_DIR
    git add $1
    cd ../
}

function git_commit {
    cd $ANSIBLE_DIR
    pwd
    git commit -m "$1"
    git push
    cd ../
}

function make_includes {
    FILES=$(python make_includes.py $1)
    for FILENAME in $FILES; do
        read -ra SPLIT <<< $(split $FILENAME ".")
        APP=${SPLIT[0]}
        DEN=${SPLIT[1]}
        
        FILE="$DEN".incl.yml
        LOCAL=roles/applications/"$APP"/templates
        DEST=$ANSIBLE_DIR/$LOCAL
        if [ -d "$DEST" ]; then
            cp "$1" "$DEST/$FILE"
            git_add "$LOCAL/$FILE"
        fi
    done
}

function delete_include {
    read -ra SPLIT <<< $(split $FILENAME ".")
    APP=${SPLIT[1]}
    DENS=("manifest" "secrets")
    for DEN in "${DENS[@]}"; do
        LOCAL=roles/applications/"$APP"/templates/"$DEN".incl.yml
    
        if [ -f "$ANSIBLE_DIR/$LOCAL" ]; then
            git_rm $LOCAL
        fi
    done
}
 
function make_vault {
    FILENAME=${1##*/}
    read -ra SPLIT <<< $(split $FILENAME ".")
    ENV=${SPLIT[0]}
    APP=${SPLIT[1]}
    DEST=$ANSIBLE_DIR/$LOCAL
    ENV="$ENV" ansible-vault encrypt --vault-password-file=pass.py $1

    FILE="$APP".yml
    LOCAL=group_vars/$ENV
    DEST=$ANSIBLE_DIR/$LOCAL
    
    if [ -d "$DEST" ]; then
        cp "$1" "$DEST/$FILE"
        git_add "$LOCAL/$FILE"
    fi
}

function delete_vault {
    read -ra SPLIT <<< $(split $FILENAME ".")
    APP=${SPLIT[1]}
    ENV=${SPLIT[0]}

    LOCAL=group_vars/"$ENV"/"$APP".yml

    if [ -f "$ANSIBLE_DIR/$LOCAL" ]; then
        git_rm $LOCAL
    fi
}

function setup_blackbox {
    echo -e $KEYPRIV >> /tmp/keypriv
    gpg --import /tmp/keypriv
    srm /tmp/keypriv
}

function process_impl {
    PLAIN="${1%.gpg}"
    make_includes $PLAIN
    make_vault $PLAIN
}

function process_added {
    echo ADDING "$@"
    for FILENAME in "$@"; do
        $BLACKBOX_PATH/blackbox_edit_start "$FILENAME"

        process_impl "$FILENAME"
        # Securely delete all plaintext configs
        $BLACKBOX_PATH/blackbox_shred_all_files
    done
}

function process_modified {
    echo MODIFIYING "$@"
    for FILENAME in "$@"; do
        $BLACKBOX_PATH/blackbox_edit_start "$FILENAME"

        process_impl "$FILENAME"
        # Securely delete all plaintext configs
        $BLACKBOX_PATH/blackbox_shred_all_files
    done
}

function process_deleted {
    echo DELETING "$@"
    for FILENAME in "$@"; do
        echo DELETE $FILENAME
        FILENAME=${FILENAME##*/}
        echo DELETE $FILENAME
        delete_include $FILENAME
        delete_vault $FILENAME
    done
}

TAGS=${CIRCLE_COMPARE_URL##*/}
PREV_TAG=${TAGS#*..}

FILTER="configs/.*\.cfg\.gpg"
# MAIN
read -ra MODIFIED <<< $(git diff --diff-filter=M --name-only $PREV_TAG HEAD | grep -o $FILTER)
read -ra ADDED <<< $(git diff --diff-filter=A --name-only $PREV_TAG HEAD | grep -o $FILTER)
read -ra DELETED <<< $(git diff --diff-filter=D --name-only $PREV_TAG HEAD | grep -o $FILTER)

CHANGED=("${ADDED[@]}" "${MODIFIED[@]}")

ADDED_CNT=$(echo ${ADDED[@]} | wc -w)
DELETED_CNT=$(echo ${DELETED[@]} | wc -w)
MODIFIED_CNT=$(echo ${MODIFIED[@]} | wc -w)

echo $MODIFIED_CNT
echo ${MODIFIED[@]}

if [ $[ADDED_CNT+MODIFIED_CNT] -ne 0 ]; then
    setup_blackbox
else
    if [ $DELETED_CNT -eq 0 ]; then
        echo "No secrets changed - Nothing to do"
        exit 0
    fi
fi

if [ $ADDED_CNT -ne 0 ]; then
    process_added ${ADDED[@]}
    printf -v msg "%s\n" "${ADDED[@]}"
    git_commit "Added new configuration $msg"
fi

if [ $MODIFIED_CNT -ne 0 ]; then
    process_modified ${MODIFIED[@]}
    printf -v msg "%s\n" "${MODIFIED[@]}"
    git_commit "Modified configuration in $msg"
fi

if [ $DELETED_CNT -ne 0 ]; then
    process_deleted ${DELETED[@]}
    printf -v msg "%s\n" "${DELETED[@]}"
    git_commit "Deleted configuration $msg"
fi

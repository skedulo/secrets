#! /usr/bin/python 
import os

arg=os.getenv('ENV')
if (arg == 'test'):
    print(os.getenv('VAULT_TEST_PASSWORD'))
elif (arg == 'prod'):
    print(os.getenv('VAULT_PROD_PASSWORD'))
else:
    print(os.getenv('VAULT_DEV_PASSWORD'))
